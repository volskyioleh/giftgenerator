package com.example.volskyioleh.project1;

import android.content.ActivityNotFoundException;
import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.Color;
import android.net.Uri;
import android.os.Bundle;
import android.preference.PreferenceManager;
import android.support.annotation.NonNull;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.View;
import android.widget.AdapterView;
import android.widget.GridView;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.adcolony.sdk.AdColony;
import com.adcolony.sdk.AdColonyInterstitial;
import com.adcolony.sdk.AdColonyInterstitialListener;
import com.applovin.adview.AppLovinAdView;
import com.applovin.adview.AppLovinIncentivizedInterstitial;
import com.applovin.sdk.AppLovinAd;
import com.applovin.sdk.AppLovinAdClickListener;
import com.applovin.sdk.AppLovinAdDisplayListener;
import com.applovin.sdk.AppLovinAdLoadListener;
import com.applovin.sdk.AppLovinAdRewardListener;
import com.applovin.sdk.AppLovinAdVideoPlaybackListener;
import com.applovin.sdk.AppLovinErrorCodes;
import com.example.volskyioleh.project1.adapters.GridAtapters;
import com.fyber.ads.AdFormat;
import com.fyber.currency.VirtualCurrencyErrorResponse;
import com.fyber.currency.VirtualCurrencyResponse;
import com.fyber.requesters.OfferWallRequester;
import com.fyber.requesters.RequestCallback;
import com.fyber.requesters.RequestError;
import com.fyber.requesters.VirtualCurrencyCallback;
import com.fyber.requesters.VirtualCurrencyRequester;

import com.offertoro.sdk.sdk.OffersInit;
import com.vungle.publisher.AdConfig;
import com.vungle.publisher.VungleAdEventListener;
import com.vungle.publisher.VunglePub;


import java.util.Map;


public class EarnTokensActivity extends AppCompatActivity implements AppLovinAdLoadListener, AppLovinAdDisplayListener, AppLovinAdClickListener, AppLovinAdVideoPlaybackListener {

    private AppLovinIncentivizedInterstitial incentivizedInterstitial;

    private SharedPreferences coinsSP;
    private Intent mOfferwallIntent;
    private TextView mCoinsTv;
    final VunglePub vunglePub = VunglePub.getInstance();
    private AppLovinAdView adView;
    private AdColonyInterstitial mAdColonyInterstitial;


    private void onLevelStart() {
        vunglePub.loadAd("DEFAULT76785");
    }

    private void onLevelComplete() {
        if (vunglePub.isAdPlayable("DEFAULT76785")) {
            vunglePub.playAd("DEFAULT76785", vunglePub.getGlobalAdConfig());
        } else {
            Toast.makeText(getApplicationContext(), "Video is not ready", Toast.LENGTH_LONG).show();
        }
    }


    RequestCallback requestCallback = new RequestCallback() {
        @Override
        public void onAdAvailable(Intent intent) {
            // Store the intent that will be used later to show the Offer Wall
            mOfferwallIntent = intent;
            Log.d("FYBER", "Offers are available");
        }

        @Override
        public void onAdNotAvailable(AdFormat adFormat) {
            // Since we don't have an ad, it's best to reset the Offer Wall intent
            mOfferwallIntent = null;
            Log.d("FYBER", "No ad available");
        }

        @Override
        public void onRequestError(RequestError requestError) {
            // Since we don't have an ad, it's best to reset the Offer Wall intent
            mOfferwallIntent = null;
            Log.d("FYBER", "Something went wrong with the request: " + requestError.getDescription());
        }
    };

    private String placementIdForLevel = "DEFAULT76785";

    private final VungleAdEventListener vungleListener = new VungleAdEventListener() {

        @Override
        public void onAdEnd(String placementReferenceId, boolean wasSuccessfulView, boolean wasCallToActionClicked) {
            // Called when user exits the ad and control is returned to your application
            // if wasSuccessfulView is true, the user watched the ad and should be rewarded
            // (if this was a rewarded ad).
            // if wasCallToActionClicked is true, the user clicked the call to action
            // button in the ad.
            if (wasSuccessfulView) {
                setCoins(20);
            }
        }

        @Override
        public void onAdStart(String placementReferenceId) {
            // Called before playing an ad
        }

        @Override
        public void onUnableToPlayAd(String placementReferenceId, String reason) {
            // Called after playAd(placementId, adConfig) is unable to play the ad
        }

        @Override
        public void onAdAvailabilityUpdate(String placementReferenceId, boolean isAdAvailable) {
            // Notifies ad availability for the indicated placement
            // There can be duplicate notifications
        }
    };

    // private void onLevelStart() {
    //       vunglePub.playAd();
    // }

    AdColonyInterstitialListener listener = new AdColonyInterstitialListener() {
        @Override
        public void onClosed(AdColonyInterstitial ad) {
            super.onClosed(ad);
            setCoins(20);
        }

        @Override
        public void onRequestFilled(AdColonyInterstitial ad) {
            /* Store and use this ad object to show your ad when appropriate */
            mAdColonyInterstitial = ad;
        }
    };


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_earn_tokens);
        GridView gridview = findViewById(R.id.gridView1);
        gridview.setAdapter(new GridAtapters(this));
        gridview.setOnItemClickListener(gridviewOnItemClickListener);
        ImageView imageView = findViewById(R.id.back_btn_earn);
        imageView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
            }
        });
        final String app_id = "59ad0123c4421b8c640013e2";
        AdColony.requestInterstitial("vzfdf03d56aaea405e95", listener);
        //   vunglePub.setEventListeners(vungleDefaultListener);
        coinsSP = PreferenceManager.getDefaultSharedPreferences(getApplicationContext());
        incentivizedInterstitial = AppLovinIncentivizedInterstitial.create(getApplicationContext());


        adView = findViewById(R.id.ad_view);
        adView.loadNextAd();
        vunglePub.addEventListeners(vungleListener);
        onLevelStart();

        AdColony.requestInterstitial("vzfdf03d56aaea405e95", listener);
        OffersInit.getInstance().create(this);
        OffersInit.getInstance().setOfferWallListener(new com.offertoro.sdk.interfaces.OfferWallListener() {
            @Override
            public void onOTOfferWallInitSuccess() {
                Log.d("TOOOORRROO", "SUCCESS");
            }

            @Override
            public void onOTOfferWallInitFail(String s) {
                Log.d("TOOOORRROO", "FAIL");
            }

            @Override
            public void onOTOfferWallOpened() {
                Log.d("TOOOORRROO", "OPENED");
            }

            @Override
            public void onOTOfferWallCredited(double v, double v1) {
                Log.d("TOOOORRROO", "REWAEDED");
                setCoins((int) v);
            }

            @Override
            public void onOTOfferWallClosed() {
                Log.d("TOOOORRROO", "CLOSED");
            }
        });


        mCoinsTv = findViewById(R.id.coins);

        OfferWallRequester.create(requestCallback)
                .request(this);
        VirtualCurrencyRequester.create(virtualCurrencyCallback)
                .request(this);
    }


    @Override
    public void onDestroy() {
        vunglePub.clearEventListeners();
        super.onDestroy();
    }

    VirtualCurrencyCallback virtualCurrencyCallback = new VirtualCurrencyCallback() {
        @Override
        public void onSuccess(VirtualCurrencyResponse virtualCurrencyResponse) {
            // Reward your user based on the deltaOfCoins parameter
            double deltaOfCoins = virtualCurrencyResponse.getDeltaOfCoins();
            setCoins((int) virtualCurrencyResponse.getDeltaOfCoins());
        }

        @Override
        public void onRequestError(RequestError requestError) {
            // No reward has been returned, so nothing can be provided to the user
            //   Log.d(TAG, "request error: " + requestError.getDescription());
        }

        @Override
        public void onError(VirtualCurrencyErrorResponse virtualCurrencyErrorResponse) {
            // No reward has been returned, so nothing can be provided to the user
            //   Log.d(TAG, "VCS error received - " + virtualCurrencyErrorResponse.getErrorMessage());
        }
    };

    public void setCoins(int coins_data) {
        int current_coins = coinsSP.getInt("COINS", 0);
        SharedPreferences.Editor ed = coinsSP.edit();
        ed.putInt("COINS", current_coins + coins_data);
        ed.apply();
        mCoinsTv.setText(String.valueOf(coinsSP.getInt("COINS", 0)));
    }

    @Override
    protected void onPause() {
        super.onPause();
        vunglePub.onPause();
    }

    @Override
    protected void onResume() {
        super.onResume();
        // MonetizationManager.createSession(getApplicationContext(), "140548", sessionListener);
        mCoinsTv.setText(Integer.toString(coinsSP.getInt("COINS", 0)));
        final String app_id = "59ad0123c4421b8c640013e2";
        incentivizedInterstitial.preload(new AppLovinAdLoadListener() {
            @Override
            public void adReceived(AppLovinAd appLovinAd) {
                System.out.println("Wahoo! VIDEO LOADED");
            }

            @Override
            public void failedToReceiveAd(int errorCode) {
                System.out.println("FAAAAAIL");
            }
        });
        adView.loadNextAd();
        vunglePub.onResume();
    }

    private GridView.OnItemClickListener gridviewOnItemClickListener = new GridView.OnItemClickListener() {
        @Override
        public void onItemClick(AdapterView<?> parent, View v, int position,
                                long id) {
            // TODO Auto-generated method stub
            // выводим номер позиции
            //mSelectText.setText(String.valueOf(position));
            switch (position) {
                case 0:
                    startActivity(mOfferwallIntent);
                    break;
                case 1:
                    startActivity(mOfferwallIntent);
                    break;
                case 2:
                    OffersInit.getInstance().showOfferWall(EarnTokensActivity.this);
                    break;
                case 3:
                    onLevelComplete();
                    break;
                case 4:
                    if (mAdColonyInterstitial != null) {
                        mAdColonyInterstitial.show();
                        AdColony.requestInterstitial("vzfdf03d56aaea405e95", listener);
                        if (mAdColonyInterstitial.getListener() != null) {
                            mAdColonyInterstitial.getListener().onExpiring(mAdColonyInterstitial);
                        }
                    } else {
                        AdColony.requestInterstitial("vzfdf03d56aaea405e95", listener);
                        Toast.makeText(getApplicationContext(), "Task is not ready", Toast.LENGTH_LONG).show();
                    }
                    break;
                case 5:
                    if (incentivizedInterstitial.isAdReadyToDisplay()) {

                        AppLovinAdRewardListener adRewardListener = new AppLovinAdRewardListener() {
                            @Override
                            public void userRewardVerified(AppLovinAd appLovinAd, Map map) {
                                Log.d("NOT READY", "FYBER");
//                    String currencyName = (String) map.get( "currency" );
//                    String amountGivenString = (String) map.get( "amount" );
                                //                           setCoins(20);

                                //map.get()
                            }

                            @Override
                            public void userOverQuota(AppLovinAd appLovinAd, Map map) {
                                Log.d("NOT READY", "FYBER");
                                //   log( "Reward validation request exceeded quota with response: " + map );
                            }

                            @Override
                            public void userRewardRejected(AppLovinAd appLovinAd, Map map) {
                                Log.d("NOT READY", "FYBER");
                            }

                            @Override
                            public void validationRequestFailed(AppLovinAd appLovinAd, int responseCode) {
                                if (responseCode == AppLovinErrorCodes.INCENTIVIZED_USER_CLOSED_VIDEO) {
                                    Log.d("NOT READY", "FYBER");
                                    // Your user exited the video prematurely. It's up to you if you'd still like to grant
                                    // a reward in this case. Most developers choose not to. Note that this case can occur
                                    // after a reward was initially granted (since reward validation happens as soon as a
                                    // video is launched).
                                } else if (responseCode == AppLovinErrorCodes.INCENTIVIZED_SERVER_TIMEOUT || responseCode == AppLovinErrorCodes.INCENTIVIZED_UNKNOWN_SERVER_ERROR) {
                                    Log.d("NOT READY", "FYBER");
                                    // Some server issue happened here. Don't grant a reward. By default we'll show the user
                                    // a alert telling them to try again later, but you can change this in the
                                    // AppLovin dashboard.
                                } else if (responseCode == AppLovinErrorCodes.INCENTIVIZED_NO_AD_PRELOADED) {
                                    Log.d("NOT READY", "FYBER");
                                    // Indicates that the developer called for a rewarded video before one was available.
                                    // Note: This code is only possible when working with rewarded videos.
                                }
                                // log( "Reward validation request failed with error code: " + responseCode );
                            }

                            @Override
                            public void userDeclinedToViewAd(AppLovinAd appLovinAd) {
                                // This method will be invoked if the user selected "no" when asked if they want to view an ad.
                                // If you've disabled the pre-video prompt in the "Manage Apps" UI on our website, then this method won't be called.
                                Log.d("NOT READY", "FYBER");
                                //   log( "User declined to view ad" );
                            }
                        };
                        incentivizedInterstitial.show(getApplicationContext(), adRewardListener, new AppLovinAdVideoPlaybackListener() {
                            @Override
                            public void videoPlaybackBegan(AppLovinAd appLovinAd) {

                            }

                            @Override
                            public void videoPlaybackEnded(AppLovinAd appLovinAd, double v, boolean b) {
                                setCoins(20);
                            }
                        });
                    }
                    break;
                case 6:
                    Intent intent = new Intent(getApplicationContext(), MainActivity.class);
                    startActivity(intent);
                    break;
                case 7:
                    Uri uri = Uri.parse("market://details?id=" + getApplicationContext().getPackageName());
                    Intent goToMarket = new Intent(Intent.ACTION_VIEW, uri);
                    // To count with Play market backstack, After pressing back button,
                    // to taken back to our application, we need to add following flags to intent.
                    goToMarket.addFlags(Intent.FLAG_ACTIVITY_NO_HISTORY |
                            Intent.FLAG_ACTIVITY_NEW_DOCUMENT |
                            Intent.FLAG_ACTIVITY_MULTIPLE_TASK);
                    try {
                        startActivity(goToMarket);
                    } catch (ActivityNotFoundException e) {
                        startActivity(new Intent(Intent.ACTION_VIEW,
                                Uri.parse("http://play.google.com/store/apps/details?id=" +  getApplicationContext().getPackageName())));
                    }
                    break;
            }
        }
    };

    @Override
    public void adClicked(AppLovinAd appLovinAd) {

    }

    @Override
    public void adDisplayed(AppLovinAd appLovinAd) {

    }

    @Override
    public void adHidden(AppLovinAd appLovinAd) {

    }

    @Override
    public void adReceived(AppLovinAd appLovinAd) {

    }

    @Override
    public void failedToReceiveAd(int i) {

    }

    @Override
    public void videoPlaybackBegan(AppLovinAd appLovinAd) {

    }

    @Override
    public void videoPlaybackEnded(AppLovinAd appLovinAd, double v, boolean b) {
        //  setCoins((int) v);
    }
}
