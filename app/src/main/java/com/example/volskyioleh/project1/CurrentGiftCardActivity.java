package com.example.volskyioleh.project1;

import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.Color;
import android.graphics.Typeface;
import android.graphics.drawable.ColorDrawable;
import android.preference.PreferenceManager;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.text.Layout;
import android.view.View;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.TextView;

import com.applovin.adview.AppLovinAdView;
import com.vungle.publisher.VunglePub;

public class CurrentGiftCardActivity extends AppCompatActivity {
    private AppLovinAdView adView;
    int price;
    final VunglePub vunglePub = VunglePub.getInstance();
    private Typeface typeface;
    private SharedPreferences coinsSP;
    private TextView tokens;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_current_gift_card);

        TextView outPutText = findViewById(R.id.gift_Tv);
        ImageView imageView = findViewById(R.id.iv1);
        ImageView imageView2 = findViewById(R.id.iv2);
        ImageView imageView3 = findViewById(R.id.iv3);
        coinsSP = PreferenceManager.getDefaultSharedPreferences(getApplicationContext());
        typeface = Typeface.createFromAsset(getAssets(), "fonts/montserrat_medium.ttf");
        tokens = findViewById(R.id.coins1);
        SharedPreferences coinsSP = PreferenceManager.getDefaultSharedPreferences(getApplicationContext());
        tokens.setText(Integer.toString(coinsSP.getInt("COINS", 0)));
        adView = findViewById(R.id.ad_view);
        adView.loadNextAd();

        outPutText.setTypeface(typeface);

        final ImageView earnCoins = findViewById(R.id.earn_new_coins);
        earnCoins.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(getApplicationContext(), EarnTokensActivity.class);
                startActivity(intent);
                finish();
            }
        });

        FrameLayout frameLayout1 = findViewById(R.id.frame1);
        FrameLayout frameLayout2 = findViewById(R.id.frame2);
        FrameLayout frameLayout3 = findViewById(R.id.frame3);

        frameLayout1.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                price = 4999;
                checkPrice(price);
            }
        });

        frameLayout2.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                price = 7999;
                checkPrice(price);
            }
        });

        frameLayout3.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                price = 12999;
                checkPrice(price);
            }
        });


        ImageView back = findViewById(R.id.back_btn_cur);
        back.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
            }
        });

        outPutText.setText(getIntent().getStringExtra("TEXT"));
        imageView.setImageResource(getIntent().getIntExtra("IMAGE", 0));
        imageView2.setImageResource(getIntent().getIntExtra("IMAGE", 0));
        imageView3.setImageResource(getIntent().getIntExtra("IMAGE", 0));
    }

    private void checkPrice(int price) {
        int currentCoins = coinsSP.getInt("COINS", 0);
        if (currentCoins < price) {
            buildAlert("Not Enough Tokens ", "Sorry, but you don't have enough tokens");
        } else {
            SharedPreferences.Editor ed = coinsSP.edit();
            ed.putInt("COINS", currentCoins - price);
            ed.apply();
            tokens.setText(String.valueOf(coinsSP.getInt("COINS", 0)));
            buildAlert("Congratulations!",
                    "You will get your 25$ Amazon Gift card to your e-mail  within 48 hours");
        }
    }

    public void buildAlert(String titleStr, String messageStr) {
        AlertDialog.Builder mBuilder = new AlertDialog.Builder(CurrentGiftCardActivity.this);
        View mView = getLayoutInflater().inflate(R.layout.alert_style, null);
        TextView title = mView.findViewById(R.id.title_text);
        title.setTypeface(typeface);
        TextView message = mView.findViewById(R.id.text_message);
        ImageView ok_btn = mView.findViewById(R.id.ok_btn);
        message.setTypeface(typeface);
        title.setText(titleStr);
        message.setText(messageStr);

        mBuilder.setView(mView);
        final AlertDialog dialog = mBuilder.create();
        dialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
        dialog.show();

        ok_btn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dialog.hide();
            }
        });
    }

    @Override
    protected void onResume() {
        super.onResume();
        vunglePub.onResume();
        adView.loadNextAd();
    }

    @Override
    protected void onPause() {
        super.onPause();
        vunglePub.onPause();
    }
}
