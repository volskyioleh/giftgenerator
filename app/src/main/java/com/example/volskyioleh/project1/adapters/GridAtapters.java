package com.example.volskyioleh.project1.adapters;

import android.content.Context;
import android.util.DisplayMetrics;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.GridView;
import android.widget.ImageView;

import com.example.volskyioleh.project1.R;

/**
 * Created by Volskyi Oleh on 14.01.2018.
 */

public class GridAtapters extends BaseAdapter {
    private Context mContext;

    public GridAtapters(Context c) {

        mContext = c;
    }

    public int getCount() {
        return mThumbIds.length;
    }

    public Object getItem(int position) {
        return mThumbIds[position];
    }

    public long getItemId(int position) {
        return position;
    }

    // create a new ImageView for each item referenced by the Adapter
    public View getView(int position, View convertView, ViewGroup parent) {


        ImageView imageView;
        if (convertView == null) {
            // if it's not recycled, initialize some attributes
            imageView = new ImageView(mContext);
          //  imageView.setLayoutParams(new GridView.LayoutParams(mElemSize, mElemSize));
            imageView.setScaleType(ImageView.ScaleType.FIT_CENTER);
            imageView.setAdjustViewBounds(true);
            imageView.setPadding(0, -10, 0, -10);
        } else {
            imageView = (ImageView) convertView;
        }

        imageView.setImageResource(mThumbIds[position]);
        return imageView;
    }

    // references to our images
    public	Integer[] mThumbIds = { R.drawable.timer, R.drawable.gift,
            R.drawable.tons, R.drawable.watch, R.drawable.daily,
            R.drawable.bonus, R.drawable.spinner1, R.drawable.rate,
          };
}

