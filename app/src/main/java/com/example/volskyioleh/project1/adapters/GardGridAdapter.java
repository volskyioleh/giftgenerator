package com.example.volskyioleh.project1.adapters;

import android.content.Context;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;

import com.example.volskyioleh.project1.R;

/**
 * Created by Volskyi Oleh on 14.01.2018.
 */

public class GardGridAdapter extends BaseAdapter {
    private Context mContext;


    public GardGridAdapter(Context c) {
        mContext = c;
    }

    public int getCount() {
        return mThumbIds.length;
    }

    public Object getItem(int position) {
        return mThumbIds[position];
    }

    public long getItemId(int position) {
        return position;
    }

    // create a new ImageView for each item referenced by the Adapter
    public View getView(int position, View convertView, ViewGroup parent) {


        ImageView imageView;
        if (convertView == null) {
            // if it's not recycled, initialize some attributes
            imageView = new ImageView(mContext);
            //  imageView.setLayoutParams(new GridView.LayoutParams(mElemSize, mElemSize));
            imageView.setScaleType(ImageView.ScaleType.FIT_CENTER);
            imageView.setAdjustViewBounds(true);
            imageView.setPadding(0, -10, 0, -10);
        } else {
            imageView = (ImageView) convertView;
        }

        imageView.setImageResource(mThumbIds[position]);
        return imageView;
    }

    // references to our images
    public	Integer[] mThumbIds = { R.drawable.amazon, R.drawable.ebay,
            R.drawable.paypal, R.drawable.xbox1, R.drawable.steam,
            R.drawable.playstation
    };
}
