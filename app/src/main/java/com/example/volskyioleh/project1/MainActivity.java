package com.example.volskyioleh.project1;

import android.animation.Animator;
import android.animation.ObjectAnimator;
import android.app.AlarmManager;
import android.app.PendingIntent;
import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.Typeface;
import android.os.Build;
import android.os.CountDownTimer;
import android.preference.PreferenceManager;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.view.animation.DecelerateInterpolator;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.adcolony.sdk.AdColony;
import com.adcolony.sdk.AdColonyInterstitial;
import com.adcolony.sdk.AdColonyInterstitialListener;
import com.applovin.adview.AppLovinAdView;
import com.applovin.adview.AppLovinIncentivizedInterstitial;
import com.applovin.adview.AppLovinInterstitialAd;
import com.applovin.adview.AppLovinInterstitialAdDialog;
import com.applovin.sdk.AppLovinAd;
import com.applovin.sdk.AppLovinAdClickListener;
import com.applovin.sdk.AppLovinAdDisplayListener;
import com.applovin.sdk.AppLovinAdLoadListener;
import com.applovin.sdk.AppLovinAdRewardListener;
import com.applovin.sdk.AppLovinAdSize;
import com.applovin.sdk.AppLovinAdVideoPlaybackListener;
import com.applovin.sdk.AppLovinErrorCodes;
import com.applovin.sdk.AppLovinSdk;

import com.vungle.publisher.VunglePub;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.Map;
import java.util.Random;
import java.util.concurrent.TimeUnit;

public class MainActivity extends AppCompatActivity implements AppLovinAdLoadListener, AppLovinAdDisplayListener, AppLovinAdClickListener, AppLovinAdVideoPlaybackListener {

    private ImageView imageRoulette;
    private static final long MILLIS_IN_HOUR = 3600 * 1000 * 3;
    final VunglePub vunglePub = VunglePub.getInstance();
    private ImageView go;
    private AppLovinAdView adView;
    private SharedPreferences coinsSP;
    private TextView tokens;
    private int reward;
    private TextView spinCount;
    private TextView mTimer;
    private AdColonyInterstitial mAdColonyInterstitial;
    private AppLovinAd currentAd;
    private AppLovinInterstitialAdDialog interstitialAd;
   // private AppLovinInterstitialAd  incentivizedInterstitial;;
    private AppLovinIncentivizedInterstitial incentivizedInterstitial;
    //   private AppLovinInterstitialAdDialog interstitialAd;
    private ImageView mResetBtn;
    private ImageView hideText;
    private CountDownTimer timer;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        final String app_id = "59ad0123c4421b8c640013e2";

        onLevelStart();
        interstitialAd = AppLovinInterstitialAd.create(AppLovinSdk.getInstance(this), this);
        adView = findViewById(R.id.ad_view);
        adView.loadNextAd();
        ImageView imageView = findViewById(R.id.back_btn);
        imageView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
            }
        });
        Typeface typeface = Typeface.createFromAsset(getAssets(), "fonts/montserrat_medium.ttf");
        spinCount = findViewById(R.id.count_tv);
        spinCount.setTypeface(typeface);
        tokens = findViewById(R.id.coins2);
        mTimer = findViewById(R.id.timer);
        mTimer.setTypeface(typeface);
        mResetBtn = findViewById(R.id.reset_btn);
        hideText = findViewById(R.id.timer_tv);
        coinsSP = PreferenceManager.getDefaultSharedPreferences(getApplicationContext());
        //  SharedPreferences.Editor ed = coinsSP.edit();
        AdColony.requestInterstitial("vzfdf03d56aaea405e95", listener);

      //  vunglePub.setEventListeners(vungleDefaultListener);
      //  incentivizedInterstitial = AppLovinInterstitialAd.create(AppLovinSdk.initializeSdk(this), getApplicationContext());
       incentivizedInterstitial = AppLovinIncentivizedInterstitial.create(getApplicationContext());
        AppLovinSdk.getInstance(getApplicationContext()).getAdService().loadNextAd(AppLovinAdSize.INTERSTITIAL, new AppLovinAdLoadListener() {
            @Override
            public void adReceived(AppLovinAd ad) {
                //log( "Interstitial Loaded" );
                currentAd = ad;

            }

            @Override
            public void failedToReceiveAd(int errorCode) {
                // Look at AppLovinErrorCodes.java for list of error codes
                //  log( "Interstitial failed to load with error code " + errorCode );
            }
        });

        showSpinCounts();
        tokens.setText(Integer.toString(coinsSP.getInt("COINS", 0)));

        imageRoulette = findViewById(R.id.spinner_imv);
        go = findViewById(R.id.spin_btn);

        if (coinsSP.getInt("SPINS", 3) <= 0) {
            changeOffUI();
            timer();
        }
        go.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                actionRoulette(imageRoulette);
            }
        });

        if (coinsSP.getInt("RESET", 0) == 1) {
            mResetBtn.setEnabled(true);
            mResetBtn.setVisibility(View.VISIBLE);
            mResetBtn.setImageResource(R.drawable.ern);
            mResetBtn.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    Intent inten = new Intent(getApplicationContext(),EarnTokensActivity.class);
                    startActivity(inten);
                }
            });
        }
        coinsSP.getInt("RESET", 0);
        mResetBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                coinsSP.getInt("RESET", 0);
                //if (vunglePub.isAdPlayable()) {
                    onLevelComplete();
             //   }
                SharedPreferences.Editor ed = coinsSP.edit();
                ed.putInt("RESET", 1);
                ed.apply();
                setNewCountsForOneSpin();
                changeOnUIfor1();
                timer.cancel();
            }
        });
    }

    @Override
    public void onDestroy() {
        vunglePub.clearEventListeners();
        super.onDestroy();
    }

   // private void onLevelStart() {
        //vunglePub.playAd();
    //}

    AdColonyInterstitialListener listener = new AdColonyInterstitialListener() {
        @Override
        public void onClosed(AdColonyInterstitial ad) {
            super.onClosed(ad);
        }

        @Override
        public void onRequestFilled(AdColonyInterstitial ad) {
            /* Store and use this ad object to show your ad when appropriate */
            mAdColonyInterstitial = ad;
        }
    };

    private void showAdColony() {
        if (mAdColonyInterstitial != null) {
            mAdColonyInterstitial.show();
            AdColony.requestInterstitial("vzfdf03d56aaea405e95", listener);
            if (mAdColonyInterstitial.getListener() != null) {
                mAdColonyInterstitial.getListener().onExpiring(mAdColonyInterstitial);
            }
        } else {
            AdColony.requestInterstitial("vzfdf03d56aaea405e95", listener);
            Toast.makeText(getApplicationContext(), "Task is not ready", Toast.LENGTH_LONG).show();
        }
    }

    public void timer() {

        Calendar calendar = Calendar.getInstance();
        long timerTime = coinsSP.getLong("DATE", 0) - calendar.getTimeInMillis();
        timer = new CountDownTimer(timerTime, 1000) {

            public void onTick(long millisUntilFinished) {
                // DateFormat formatter = new SimpleDateFormat("HH:mm:ss");
                String hms = String.format("%02d:%02d:%02d", TimeUnit.MILLISECONDS.toHours(millisUntilFinished),
                        TimeUnit.MILLISECONDS.toMinutes(millisUntilFinished)
                                - TimeUnit.HOURS.toMinutes(TimeUnit.MILLISECONDS.toHours(millisUntilFinished)),
                        TimeUnit.MILLISECONDS.toSeconds(millisUntilFinished)
                                - TimeUnit.MINUTES.toSeconds(TimeUnit.MILLISECONDS.toMinutes(millisUntilFinished)));

//                System.out.println(hms);
//                String dateFormatted = formatter.format(millisUntilFinished);
                mTimer.setText(hms);
            }

            public void onFinish() {
                changeOnUI();
                showSpinCounts();
            }
        };
        timer.start();
    }


    public void changeOnUI() {
        go.setEnabled(true);
       // setNewCounts();
        go.setVisibility(View.VISIBLE);
        setNewCounts();

        imageRoulette.setImageResource(R.drawable.spinner);
        mTimer.setVisibility(View.INVISIBLE);
        mResetBtn.setVisibility(View.INVISIBLE);
        hideText.setVisibility(View.INVISIBLE);


    }
    public void changeOnUIfor1() {
        go.setEnabled(true);
       // setNewCounts();
        go.setVisibility(View.VISIBLE);
        //setNewCounts();
        spinCount.setText("1");
        imageRoulette.setImageResource(R.drawable.spinner);
        mTimer.setVisibility(View.INVISIBLE);
        hideText.setVisibility(View.INVISIBLE);
        mResetBtn.setVisibility(View.INVISIBLE);

    }

    public void changeOffUI() {
        go.setEnabled(false);
        go.setVisibility(View.INVISIBLE);
        imageRoulette.setImageResource(R.drawable.spinner_empty);
        mTimer.setVisibility(View.VISIBLE);
        if (coinsSP.getInt("RESET", 0) == 1) {
            mResetBtn.setEnabled(true);
            mResetBtn.setVisibility(View.VISIBLE);
            mResetBtn.setImageResource(R.drawable.ern);
            mResetBtn.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    Intent inten = new Intent(getApplicationContext(),EarnTokensActivity.class);
                    startActivity(inten);
                }
            });
        } else {
            mResetBtn.setVisibility(View.VISIBLE);
            mResetBtn.setEnabled(true);
        }
        hideText.setVisibility(View.VISIBLE);
    }

    @Override
    protected void onPause() {
        super.onPause();
        vunglePub.onPause();
    }

    public void setCoins(int coins_data) {
        int current_coins = coinsSP.getInt("COINS", 0);
        SharedPreferences.Editor ed = coinsSP.edit();
        ed.putInt("COINS", current_coins + coins_data);
        ed.apply();
        tokens.setText(String.valueOf(coinsSP.getInt("COINS", 0)));
    }

    public void setNewCounts() {
          SharedPreferences.Editor ed = coinsSP.edit();
        ed.putInt("SPINS", 3);
        ed.apply();
        spinCount.setText(String.valueOf(coinsSP.getInt("SPINS", 3)));
    }

    public void setNewCountsForOneSpin() {
        SharedPreferences.Editor ed = coinsSP.edit();
        ed.putInt("SPINS", 1);
        ed.apply();
        spinCount.setText(String.valueOf(coinsSP.getInt("SPINS", 3)));
    }

    public void getSpinCounts() {
        int trys = coinsSP.getInt("SPINS", 3);
        SharedPreferences.Editor ed = coinsSP.edit();
        ed.putInt("SPINS", trys - 1);
        ed.apply();
        spinCount.setText(String.valueOf(coinsSP.getInt("SPINS", 3)));
    }

    public void showSpinCounts() {
     //   if (coinsSP.getInt("SPINS", 3) >= 0) {
            spinCount.setText(String.valueOf(coinsSP.getInt("SPINS", 3)));
    //    } else {
      //      spinCount.setText("0");
    //    }
    }

    @Override
    protected void onResume() {
        super.onResume();
        vunglePub.onResume();
        incentivizedInterstitial.preload(new AppLovinAdLoadListener() {
            @Override
            public void adReceived(AppLovinAd appLovinAd) {
                System.out.println("Wahoo! VIDEO LOADED");
            }

            @Override
            public void failedToReceiveAd(int errorCode) {
                System.out.println("FAAAAAIL");
            }
        });
        final String app_id = "59ad0123c4421b8c640013e2";

        AdColony.requestInterstitial("vzfdf03d56aaea405e95", listener);
        adView.loadNextAd();
    }



    public void actionRoulette(View view) {
        int corner = 360 / 10; // corner for point
        int randPosition = corner * new Random().nextInt(10) + corner / 2; // random point
        int MIN = 3; // min rotation
        int MAX = 9; // max rotation
        long TIME_IN_WHEEL = 1000;  // time in one rotation
        int pos = 0;
        int randChance = new Random().nextInt(100) + 1;
        if (randChance <= 75) {
            reward = 50;
            pos = 3;
        } else if (randChance > 75 && randChance <= 92) {
            reward = 100;
            pos = 2;
        } else if (randChance > 92 && randChance <= 95) {
            reward = 200;
            pos = 1;
        } else if (randChance > 95 && randChance <= 97) {
            reward = 300;
            pos = 0;
        } else if (randChance > 97 && randChance <= 99) {
            reward = 400;
            pos = 9;
        } else if (randChance == 100) {
            reward = 500;
            pos = 8;
        }


        int randRotation = MIN + new Random().nextInt(MAX - MIN); // random rotation
        int truePosition = (corner * pos + corner / 2) + randRotation * 360; // randRotation * 360 + randPosition;
        long totalTime = TIME_IN_WHEEL * randRotation + (TIME_IN_WHEEL / 360) * (pos + corner / 2);

        ObjectAnimator animator = ObjectAnimator.ofFloat(view, "rotation", 0f, (float) truePosition);
        animator.setDuration(totalTime);
        animator.setInterpolator(new DecelerateInterpolator());
        animator.addListener(new Animator.AnimatorListener() {
            @Override
            public void onAnimationStart(Animator animator) {
                go.setEnabled(false);
            }

            @Override
            public void onAnimationEnd(Animator animator) {
                go.setEnabled(true);
                setCoins(reward);
                getSpinCounts();
                if (coinsSP.getInt("SPINS", 3) == 2) {
//                    vunglePub.playAd();
                    showAdColony();
                } else if (coinsSP.getInt("SPINS", 3) == 1) {
                    if (currentAd != null) {
                    /*
                     NOTE: We recommend the use of placements (AFTER creating them in your dashboard):

                     interstitialAd.show( "SINGLE_INSTANCE_SCREEN" );

                     To learn more about placements, check out https://applovin.com/integration#androidPlacementsIntegration
                    */
                        interstitialAd.showAndRender(currentAd);
                    }
                } else if(coinsSP.getInt("SPINS", 3) == 0){
                    if (incentivizedInterstitial.isAdReadyToDisplay()) {
                        incentivizedInterstitial.show(getApplicationContext(),null);
                    }
                }

                if (coinsSP.getInt("SPINS", 3) <= 0) {
                    changeOffUI();
                    SharedPreferences.Editor ed = coinsSP.edit();
                    Calendar calendar = Calendar.getInstance();
                    calendar.add(Calendar.MINUTE, 2);
                    //calendar.add(Calendar.HOUR, 3);
                    coinsSP.getLong("DATE", 0);
                    ed.putLong("DATE", calendar.getTimeInMillis());
                    ed.apply();
                    timer();

                    AlarmManager alarmManager = (AlarmManager) getSystemService(ALARM_SERVICE);

                    Intent notificationIntent = new Intent(getApplicationContext(), AlarmReceiver.class);
                    PendingIntent broadcast = PendingIntent.getBroadcast(getApplicationContext(), 100, notificationIntent,
                            PendingIntent.FLAG_UPDATE_CURRENT);
                    if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.KITKAT) {
                        alarmManager.setExact(AlarmManager.RTC_WAKEUP, calendar.getTimeInMillis(), broadcast);
                    }
                }
            }

            @Override
            public void onAnimationCancel(Animator animator) {

            }

            @Override
            public void onAnimationRepeat(Animator animator) {

            }
        });
        animator.start();

    }

    private void onLevelStart() {
        vunglePub.loadAd("DEFAULT76785");
    }

    private void onLevelComplete() {
        if (vunglePub.isAdPlayable("DEFAULT76785")) {
            vunglePub.playAd("DEFAULT76785", vunglePub.getGlobalAdConfig());
        }
    }

    @Override
    public void adClicked(AppLovinAd appLovinAd) {

    }

    @Override
    public void adDisplayed(AppLovinAd appLovinAd) {

    }

    @Override
    public void adHidden(AppLovinAd appLovinAd) {

    }

    @Override
    public void adReceived(AppLovinAd appLovinAd) {

    }

    @Override
    public void failedToReceiveAd(int i) {

    }

    @Override
    public void videoPlaybackBegan(AppLovinAd appLovinAd) {

    }

    @Override
    public void videoPlaybackEnded(AppLovinAd appLovinAd, double v, boolean b) {

    }
}

