package com.example.volskyioleh.project1;

import android.annotation.SuppressLint;
import android.app.AlarmManager;
import android.app.PendingIntent;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.Color;
import android.graphics.Typeface;
import android.graphics.drawable.ColorDrawable;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.os.Build;
import android.preference.PreferenceManager;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;

import android.widget.ImageView;
import android.widget.TextView;


import com.adcolony.sdk.AdColony;
import com.applovin.adview.AppLovinAdView;
import com.fyber.Fyber;
import com.offertoro.sdk.OTOfferWallSettings;
import com.vungle.publisher.VungleInitListener;
import com.vungle.publisher.VunglePub;


import java.util.Calendar;
import java.util.Map;

public class Main3Activity extends AppCompatActivity  {


    public static final String YOUR_OW_SECRET_KEY = "8907dc300b6c5736f284f9e4b4edc2d3"; //set your value
    public static final String YOUR_OW_APP_ID = "3526"; //set your value
    public static final String YOUR_OW_USER_ID = "4746"; //set your value
    final VunglePub vunglePub = VunglePub.getInstance();
    private AppLovinAdView adView;
    private SharedPreferences coinsSP;
    private TextView tokens;
    private Typeface typeface;

//    private SessionListener sessionListener = new SessionListener() {
//        @Override
//        public void createSessionCompleted(boolean success, boolean isOfferWallEnabled, String sessionId) {
//            if (success) {
//                // a session with our servers was established successfully.
//                // the app is now ready to show ads.
//                System.out.println("Wahoo! Now I'm ready to show an ad.");
//            } else {
//                // establishing a session with our servers failed;
//                // the app will be unable to show ads until a session is established
//                System.out.println("Oh no! Something isn't set up correctly - re-read the documentation or ask customer support for some help - https://selfservice.nativex.com/Help");
//            } if (isOfferWallEnabled) {
//                // a session with our servers was established successfully.
//                // the app is now ready to show ads.
//                System.out.println("EEEEEBOOOOY");
//            } else {
//                // establishing a session with our servers failed;
//                // the app will be unable to show ads until a session is established
//                System.out.println("NOOOOOOOOOOOO");
//            }
//        }
//    };

    @SuppressLint("SetTextI18n")
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main3);
        ImageView mail = findViewById(R.id.close_img);
        mail.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(getApplicationContext(), SingInActivity.class);
                startActivity(intent);
            }
        });

        tokens = findViewById(R.id.tokensTv);
        coinsSP = PreferenceManager.getDefaultSharedPreferences(getApplicationContext());
        tokens.setText(Integer.toString(coinsSP.getInt("COINS", 0)));
        // coinsSP.getInt("COINS", 0);

        typeface = Typeface.createFromAsset(getAssets(), "fonts/montserrat_medium.ttf");
        ImageView earnTokens_btn = findViewById(R.id.earn_coins_btn2);
        ImageView getCards = findViewById(R.id.get_gift_card_btn);
        ImageView changeAcc = findViewById(R.id.change_ac);
        changeAcc.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                //  buildAlert();
            }
        });
        TextView email = findViewById(R.id.email_Tv);
        if (!isOnline()) {
            buildAlert();
        }

        email.setTypeface(typeface);
        tokens.setTypeface(typeface);

        final String app_id = "59ad0123c4421b8c640013e2";
      //  vunglePub.init(this, app_id);
        vunglePub.init(this,app_id,new String[] { "DEFAULT76785" },new VungleInitListener() {
            @Override
            public void onSuccess() {
                Log.d("FYBER", "No ad available");
            }
            @Override
            public void onFailure(Throwable e){
                Log.d("FYBER", "No ad available");
            }
        });
        AdColony.configure(this, "app68ab01ff3f554d6494", "vzfdf03d56aaea405e95");
        OTOfferWallSettings.getInstance().configInit(YOUR_OW_APP_ID,
                YOUR_OW_SECRET_KEY, YOUR_OW_USER_ID);


        adView = findViewById(R.id.ad_view);
        adView.loadNextAd();

        earnTokens_btn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent1 = new Intent(getApplicationContext(), EarnTokensActivity.class);
                startActivity(intent1);
            }
        });
        getCards.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(getApplicationContext(), Main2Activity.class);
                startActivity(intent);
            }
        });



        AlarmManager alarmManager = (AlarmManager) getSystemService(ALARM_SERVICE);
        Calendar calendar = Calendar.getInstance();
        calendar.add(Calendar.MINUTE,5);
       // calendar.add(Calendar.DAY_OF_MONTH, 1);
       // calendar.add(Calendar.SECOND,20);
        Intent notificationIntent = new Intent(this, AlarmReceiver2.class);
        PendingIntent broadcast = PendingIntent.getBroadcast(this, 100, notificationIntent,
                PendingIntent.FLAG_UPDATE_CURRENT);
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.KITKAT) {
            alarmManager.setExact(AlarmManager.RTC_WAKEUP, calendar.getTimeInMillis(), broadcast);
        }
    }

    public void buildAlert() {
        AlertDialog.Builder mBuilder = new AlertDialog.Builder(Main3Activity.this);
        View mView = getLayoutInflater().inflate(R.layout.alert_style, null);
        TextView title = mView.findViewById(R.id.title_text);
        title.setTypeface(typeface);
        TextView message = mView.findViewById(R.id.text_message);
        ImageView ok_btn = mView.findViewById(R.id.ok_btn);
        message.setTypeface(typeface);
        title.setText("No Internet Connection");
        title.setTextSize(18);
        message.setText("Please Check your Internet Connection");

        mBuilder.setView(mView);
        final AlertDialog dialog = mBuilder.create();
        dialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
        dialog.show();

        ok_btn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dialog.hide();
            }
        });
    }

    public boolean isOnline() {
        ConnectivityManager cm =
                (ConnectivityManager) getSystemService(Context.CONNECTIVITY_SERVICE);
        NetworkInfo netInfo = cm.getActiveNetworkInfo();
        return netInfo != null && netInfo.isConnectedOrConnecting();
    }

    @Override
    protected void onResume() {
        super.onResume();
        vunglePub.onResume();
        adView.loadNextAd();
        tokens.setText(Integer.toString(coinsSP.getInt("COINS", 0)));
        Fyber.with("107173", this)
                .withSecurityToken("631267ded37db5b137bb954436c78285")
                .start();
    }

    @Override
    protected void onPause() {
        super.onPause();
        vunglePub.onPause();
    }


}


