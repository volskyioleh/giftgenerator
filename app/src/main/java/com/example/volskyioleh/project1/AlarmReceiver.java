package com.example.volskyioleh.project1;

import android.app.Notification;
import android.app.NotificationChannel;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Build;
import android.preference.PreferenceManager;
import android.support.v4.app.TaskStackBuilder;

import com.example.volskyioleh.project1.MainActivity;
import com.example.volskyioleh.project1.R;

import static android.app.NotificationManager.IMPORTANCE_DEFAULT;

/**
 * Created by Volskyi Oleh on 16.01.2018.
 */

public class AlarmReceiver extends BroadcastReceiver {
    public static final String ANDROID_CHANNEL_ID = "com.example.volskyioleh.psntest.ANDROID";

    @Override
    public void onReceive(Context context, Intent intent) {
        Intent notificationIntent = new Intent(context, Main3Activity.class);

        TaskStackBuilder stackBuilder = TaskStackBuilder.create(context);
        stackBuilder.addParentStack(MainActivity.class);
        stackBuilder.addNextIntent(notificationIntent);
        PendingIntent pendingIntent = stackBuilder.getPendingIntent(0, PendingIntent.FLAG_UPDATE_CURRENT);

        Notification.Builder builder = new Notification.Builder(context);

        Notification notification = builder.setContentTitle("Free Spins are Available")
                .setContentText("You have 3 new spins, come back and win Gift Cards!")
                .setSmallIcon(R.mipmap.icon_app)
                .setContentIntent(pendingIntent).build();

        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
            builder.setChannelId(ANDROID_CHANNEL_ID);
        }

        SharedPreferences coinsSP = PreferenceManager.getDefaultSharedPreferences(context);
        SharedPreferences.Editor ed = coinsSP.edit();
        ed.putInt("SPINS", 3);
        ed.putInt("RESET", 0);
        ed.apply();



        NotificationManager notificationManager = (NotificationManager) context.getSystemService(Context.NOTIFICATION_SERVICE);

        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
            NotificationChannel channel = new NotificationChannel(
                    ANDROID_CHANNEL_ID,
                    "NotificationDemo",
                    IMPORTANCE_DEFAULT
            );
            notificationManager.createNotificationChannel(channel);
        }
      //  notificationManager.cancel(0); // if there is already a notification, in the case of the user in the application, it will change
// and a new delay is created for 24 hours.
        notificationManager.notify(0, notification);
    }


}
