package com.example.volskyioleh.project1;

import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.Typeface;
import android.preference.PreferenceManager;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.AdapterView;
import android.widget.GridView;
import android.widget.ImageView;
import android.widget.TextView;

import com.applovin.adview.AppLovinAdView;
import com.example.volskyioleh.project1.adapters.GardGridAdapter;
import com.vungle.publisher.VunglePub;

import java.util.Calendar;
import java.util.Date;

public class Main2Activity extends AppCompatActivity {
    final VunglePub vunglePub = VunglePub.getInstance();
    private AppLovinAdView adView;
    private TextView tokens;
    private SharedPreferences coinsSP;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main2);

        adView = findViewById(R.id.ad_view);
        adView.loadNextAd();
        tokens = findViewById(R.id.coins3);
        coinsSP = PreferenceManager.getDefaultSharedPreferences(getApplicationContext());
        tokens.setText(Integer.toString(coinsSP.getInt("COINS", 0)));
        GridView gridview = findViewById(R.id.gridView2);
        gridview.setAdapter(new GardGridAdapter(this));
        gridview.setOnItemClickListener(gridviewOnItemClickListener);
        Typeface typeface = Typeface.createFromAsset(getAssets(), "fonts/montserrat_medium.ttf");
        TextView textView = findViewById(R.id.titile1);
        textView.setTypeface(typeface);
        ImageView imageView = findViewById(R.id.back_btn2);
        imageView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
            }
        });


        final ImageView earnCoins = findViewById(R.id.earn_coins_btn1);
        earnCoins.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(getApplicationContext(), EarnTokensActivity.class);
                startActivity(intent);
                finish();
            }
        });
    }

    @Override
    protected void onPause() {
        super.onPause();
        vunglePub.onPause();
    }

    @Override
    protected void onResume() {
        super.onResume();
        adView.loadNextAd();
        vunglePub.onResume();
        tokens.setText(Integer.toString(coinsSP.getInt("COINS", 0)));
    }


    private GridView.OnItemClickListener gridviewOnItemClickListener = new GridView.OnItemClickListener() {

        @Override
        public void onItemClick(AdapterView<?> parent, View v, int position,
                                long id) {

            switch (position) {
                case 0:
                    startCurrentGiftActivity(R.drawable.amazon, "Amazon Gift Card");
                    break;
                case 1:
                    startCurrentGiftActivity(R.drawable.ebay, "Ebay Gift Card");
                    break;
                case 2:
                    startCurrentGiftActivity(R.drawable.paypal, "PayPal Gift Card");
                    break;
                case 3:
                    startCurrentGiftActivity(R.drawable.xbox1, "Xbox Gift Card");
                    break;
                case 4:
                    startCurrentGiftActivity(R.drawable.steam, "Walmart Gift Card");
                    break;
                case 5:
                    startCurrentGiftActivity(R.drawable.playstation, "Playstation Gift Card");
                    break;
            }
            // TODO Auto-generated method stub
            // выводим номер позиции
            //mSelectText.setText(String.valueOf(position));
        }
    };

    public void startCurrentGiftActivity(int imageID, String text) {
        Intent intent = new Intent(getApplicationContext(), CurrentGiftCardActivity.class);
        intent.putExtra("IMAGE", imageID);
        intent.putExtra("TEXT", text);
        startActivity(intent);
    }

}






















